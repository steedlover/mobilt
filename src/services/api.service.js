import _ from '../../node_modules/underscore/underscore-min';
import angular from 'angular';

class ApiService {

  constructor(ConfigService, $rootScope, $http, $q) {
    'ngInject';
    this.config = ConfigService;
    this.data = [];
    this.dataIsReady = false;
    this.http = $http;
    this.promise = $q;
    this.rootScope = $rootScope;

    // We should start to get the data
    this.getData().then(data => {
      this.data = data;
      this.rootScope.$broadcast('dataReady', {err: ''});
    }).catch(_err => {
      this.rootScope.$broadcast('dataReady', {err: _err});
    });
  }

  onDataReady() {
    return this.promise((resolve, reject) => {
      this.rootScope.$on('dataReady', (e, extra) => {
        if (extra.err === '') {
          resolve(this.data);
        } else {
          reject(extra.err);
        }
        this.dataIsReady = true;
      });
    });
  }

  getData() {
    let _url = this.config.apiUrl + this.config.dataFile;
    return this.promise((resolve, reject) => {
      this.http.get(_url).then(res => {
        // The thing is the `isObject` function of the ubderscore packet
        // returns true if res is either object or array
        // but in the object case it will not be having the `length` property
        // and this line will throw the error with crash
        if (_.isObject(res) && Array.isArray(res.data) && res.data.length > 0) {
          resolve(res.data);
        } else {
          reject('Wrong format of the server reply');
        }
      }).catch(reject);
    });
  }

}

ApiService.$inject = ['ConfigService', '$rootScope', '$http', '$q'];

export default angular.module('services.api.service', [])
  .service('ApiService', ApiService)
  .name;
