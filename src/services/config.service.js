import angular from 'angular';

class ConfigService {

  constructor() {
    'ngInject';
    this.apiUrl = '/json/';
    this.dataFile = 'sample.json';
  }

}

ConfigService.$inject = ['$rootScope'];

export default angular.module('services.config-service', [])
  .service('ConfigService', ConfigService)
  .name;
