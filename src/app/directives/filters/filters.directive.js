'use strict';

import angular from 'angular';

function filters() {
  return {
    restrict: 'E',
    scope: {
      clickFunc: '&',
      nums: '='
    },
    template: require('./filters.directive.html'),
    link: function (scope) {
      scope.filterClick = function(type) {
        type = type ? type : '';
        scope.clickFunc({type: type});
      };
    }
  };
}

export default angular.module('directives.filters', [])
  .directive('filters', filters)
  .name;
