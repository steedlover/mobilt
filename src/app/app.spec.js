'use strict';

import app from './app';

describe('IndexController', () => {

  let ctrl;
  let d;
  let ApiService;
  let scope;
  let $httpBackend;

  beforeEach(() => {
    angular.mock.module(app);

    angular.mock.inject(($q, _ApiService_) => {
      d = $q.defer();
      ApiService = _ApiService_;
      spyOn(ApiService, 'onDataReady').and.returnValue(d.promise);
    });

    angular.mock.inject(($injector, $rootScope, $controller) => {
      scope = $rootScope;
      $httpBackend = $injector.get('$httpBackend');
      // Here I have to use external package
      // otherwise I get this error: 'unexpected get '
      var valid_respond = readJSON('src/public/json/sample.json');
      $httpBackend.whenGET(/.*/).respond(valid_respond);
      ctrl = $controller('IndexController', {
        scope: scope
      });
    });
  });

  // Simple test for controller init testing
  it('should contain the starter name', () => {
    expect(ctrl.name).toBe('Index');
  });

  // Testing the function for managing with dates
  it('Should be a proper date', () => {
    expect(ctrl.getFullDate('25.05.2012.')).toBe('25.05.2012'); // 25.05.2012. => 25.05.2012
  });

  // Check the result of data getting from the service
  describe('Asyn call', () => {
    it('should call onDataReady on ApiService', () => {
      expect(ApiService.onDataReady).toHaveBeenCalled();
      expect(ApiService.onDataReady.calls.count()).toBe(1);
    });
    it('should do something on success', () => {
      d.resolve([
        {
          id: '123'
        },{
          id: '456'
        }
      ]);
      scope.$digest();
      // Check for state on success.
      // and testing properties
      expect(ctrl.error).toBe('');
      expect(ctrl.dataArr.length).toBe(2);
    });
  });

});
