'use strict';

import './index.styl';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import filters from '../../directives/filters/filters.directive';

import routing from './index.routes';
import IndexController from './index.controller';

export default angular.module('app.index', [uirouter, filters])
  .config(routing)
  .controller('IndexController', IndexController)
  .name;
