import _ from '../../../../node_modules/underscore/underscore-min';

export default class IndexController {

  constructor(ApiService) {
    this.error = '';
    this.serv = ApiService;
    this.name = 'Index';

    this.finishedArr = []; // Finished matches
    this.liveArr = []; // Inprogress
    this.upcomingArr = []; // Upcoming

    // If data is not ready yet we should subscribe for this object updating
    if (this.serv.dataIsReady === true) {
      this.init(this.serv.data);
    } else {
      this.serv.onDataReady().then(res => this.init(res)).catch(err => this.error = err);
    }
  }

  init(data) {
    // Here we got the referrence for the real data array in the service
    this.dataArr = data;
    this.sortData(data);
    this.setFilter();
  }

  sortData(data) {
    data.forEach((e) => {
      if (_.isObject(e.status) && e.status.type) {
        if (e.status.type === 'finished') {
          this.finishedArr.push(e);
        }
        if (e.status.type === 'inprogress') {
          this.liveArr.push(e);
        }
        if (e.status.type === 'notstarted') {
          this.upcomingArr.push(e);
        }
      }

      this.elemNumbers = {
        all: this.dataArr.length,
        finished: this.finishedArr.length,
        live: this.liveArr.length,
        upcoming: this.upcomingArr.length
      };

    });
  }

  setFilter(type) {
    if (!type) {
      this.displayed = this.dataArr;
    }
    if (type === 'finished') {
      this.displayed = this.finishedArr;
    }
    if (type === 'live') {
      this.displayed = this.liveArr;
    }
    if (type === 'upcoming') {
      this.displayed = this.upcomingArr;
    }
  }


  getFullDate(ymd) {
    let ymdArr = ymd.split('.');
    let result = '';
    if (ymdArr.length >= 2) {
      let date = ymdArr[0];
      let month = ymdArr[1];
      let year = ymdArr[2];
      result = [date, month, year].join('.');
    }
    return result;
  }


}

IndexController.$inject = ['ApiService'];
