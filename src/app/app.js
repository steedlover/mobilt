'use strict';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
import '../style/styles.styl';

import routing from '../app.config';

import index from './components/index';

import ConfigService from '../services/config.service';
import ApiService from '../services/api.service';

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [uirouter, index, ConfigService, ApiService])
  .config(routing);

export default MODULE_NAME;
